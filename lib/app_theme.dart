import 'package:flutter/material.dart';

class AppTheme {

  AppTheme._();

  static const Color nearlyWhite = Color(0xFFFAFAFA);
  static const Color white = Color(0xFFFFFFFF);
  static const Color background = Color(0xFF000000);
  static const Color nearlyDarkBlue = Color(0xFF2633C5);

  static const Color barNav = Color(0xFFFFFFFF);
  static const Color barNavBackground = Color(0xB2000000);

  static const Color nearlyBlue = Color(0xFF00B6F0);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color light_grey = Color(0xFF5B5656);


  static const Color purpleOpacity = Color(0xB88500FF);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color spacer = Color(0xFFF2F2F2);

  static const String fontName = 'Roboto';

  static const TextStyle title = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 42,
    letterSpacing: 0.18,
    color: Colors.green,
  );

}