import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:smartswitch/app_theme.dart';
import 'package:smartswitch/views/layouts/singup_layout.dart';
import 'package:url_launcher/url_launcher.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final Uri _url = Uri.parse('https://native-studio.be');

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool _loading = false;

  String errorCode = "";

  void _singIn() async {
    _loading = true;
    errorCode = "";
    setState(() {});

    late User user;

    try {
      user = (await _auth.signInWithEmailAndPassword(email: _emailController.text, password: _passwordController.text)).user!;

      _loading = false;

      setState(() {});
      if (user != null) {
        Navigator.of(context).pushNamed('/home');
      }
    } catch (error) {
      if (error.toString().contains('email address')) {
        errorCode = "L'adresse e-mail est mal formatée";
      } else if (error.toString().contains('password is invalid')) {
        errorCode = 'Le mot de passe est invalide';
      }
      _loading = false;
      setState(() {});
    }
  }

  Widget buildImage(int index) {
    String imageUrl = "";

    switch (index) {
      case 1:
        imageUrl =
            "https://cdn.pixabay.com/photo/2017/08/04/21/52/earth-2581631_960_720.jpg";
        break;
      case 2:
        imageUrl =
            "https://cdn.pixabay.com/photo/2017/11/19/01/41/energy-revolution-2961681_960_720.jpg";
        break;
      case 3:
        imageUrl =
            "https://cdn.pixabay.com/photo/2018/12/14/11/48/smart-3874907_960_720.jpg";
        break;
      default:
        imageUrl =
            "https://cdn.pixabay.com/photo/2020/05/31/19/53/light-bulb-5244001_960_720.jpg";
        break;
    }

    return Container(
      color: Colors.grey,
      width: double.infinity,
      child: Image.network(
        imageUrl,
        fit: BoxFit.cover,
      ),
    );
  }

  Future<void> _launchUrl() async {
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            buildTitles(),
            Container(
              padding: EdgeInsets.only(left: 20, right: 30),
              child: Column(
                children: <Widget>[
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        buildMailField(),
                        SizedBox(height: 20),
                        buildPasswordField(),
                      ],
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(errorCode, style: TextStyle(color: Colors.red)),
                  buildForgotPasswordField(),
                  SizedBox(height: 20),
                  _loading ? CircularProgressIndicator() : buildLoginButton(),
                  SizedBox(height: 15),
                  buildRegisterButton(context),
                  SizedBox(height: 30),
                  buildCarousel(),
                  SizedBox(height: 10),
                  buildPolicy(),
                ],
              ),
            )
          ],
        ));
  }

  Widget buildForgotPasswordField() {
    return Container(
      alignment: Alignment(1, 0),
      padding: EdgeInsets.only(top: 15, left: 20),
      child: InkWell(
        child: Text(
          'Mot de passe oublié ?',
          style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontFamily: 'Montserrat',
              decoration: TextDecoration.underline),
        ),
      ),
    );
  }

  Widget buildRegisterButton(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        InkWell(
          onTap: () {
            //open register page
            Navigator.push(context, MaterialPageRoute(builder: (context) => SignupPage()));

          },
          child: Text("S'enregistrer",
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline)),
        )
      ],
    );
  }

  Widget buildMailField() {
    return TextFormField(
      controller: _emailController,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'champ requis';
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: 'Identifiant',
          labelStyle: TextStyle(
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.bold,
              color: Colors.grey),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
          )),
    );
  }

  Widget buildPasswordField() {
    return TextFormField(
      controller: _passwordController,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'champ requis';
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: 'Mot de passe',
          labelStyle: TextStyle(
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.bold,
              color: Colors.grey),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
          )),
      obscureText: true,
    );
  }

  Widget buildLoginButton() {
    return InkWell(
      onTap: () async {
        if (_formKey.currentState!.validate()) {
          setState(() {});
          _singIn();
        }
      },
      child: Container(
        height: 40,
        child: Material(
          borderRadius: BorderRadius.circular(20),
          shadowColor: Colors.grey,
          color: Colors.black,
          elevation: 7,
              child: Center(
                  child: Text('Connexion',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Montserrat')))),
      ),
    );
  }

  Widget buildTitles() {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(15, 50, 0, 0),
            child: Center(
              child: Text(
                "SmartSwitch",
                style: AppTheme.title,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(15, 140, 0, 0),
            child: Text("Connexion",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 30,
                    fontWeight: FontWeight.bold)),
          )
        ],
      ),
    );
  }

  Widget buildCarousel() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20.0),
      child: SizedBox(
        width: 345,
        child: CarouselSlider.builder(
          itemCount: 4,
          options:
              CarouselOptions(height: 220, autoPlay: true, viewportFraction: 1),
          itemBuilder: (context, index, realIndex) {
            return buildImage(index);
          },
        ),
      ),
    );
  }

  Widget buildPolicy() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        InkWell(
          onTap: () {
            _launchUrl();
          },
          child: Text("privacy policy",
              style: TextStyle(
                  color: Colors.blueGrey,
                  fontSize: 15,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline)),
        )
      ],
    );
  }
}
