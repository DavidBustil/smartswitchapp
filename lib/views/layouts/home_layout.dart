import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smartswitch/views/layouts/switch_overview_layout.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final CollectionReference _switches = FirebaseFirestore.instance.collection('switches');
  late CollectionReference _consumptions;

  @override
  void initState() {
    super.initState();

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom]);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.black,
        body: Container(
          padding: EdgeInsets.only(left: 30, top: 10, right: 30),
          child: Column(
            children: [
              buildTitle(),
              Expanded(
                child: StreamBuilder(
                    stream: _switches.where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid).snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasData) {
                        return buildInterface(snapshot);
                      } else {
                        return Center(
                          child: new CircularProgressIndicator(),
                        );
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );

    /*
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        resizeToAvoidBottomInset: true,
        extendBody: true,
        body: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              buildTitle(),
              Container(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    StreamBuilder(
                        stream: _switches.snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.hasData) {
                            return ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (context, index) {
                                final DocumentSnapshot documentSnapshot =
                                    snapshot.data!.docs[index];
                                return buildInterface(documentSnapshot, index);
                              },
                            );
                          } else {
                            return Center(
                              child: new CircularProgressIndicator(),
                            );
                          }
                        }),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );*/
  }

  Widget buildTitle() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.lightbulb,
                color: Colors.green,
                size: 40,
              ),
              Text('Smart Switch',
                  style: TextStyle(
                      color: Colors.green,
                      fontSize: 28,
                      fontWeight: FontWeight.bold)),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildInterface(AsyncSnapshot<QuerySnapshot> querySnapshot) {
    return Container(
      child: Column(
        children: [
          buildSwitchList(querySnapshot),
          SizedBox(height: 20),
          buildSwitchHistory(),
        ],
      ),
    );
  }

  Widget buildSwitchList(AsyncSnapshot<QuerySnapshot> querySnapshot) {
    return Container(
      height: 350,
      child: ListView.builder(
        itemCount: querySnapshot.data!.docs.length,
        itemBuilder: (context, index) {
          final DocumentSnapshot documentSnapshot =
              querySnapshot.data!.docs[index];
          return buildSwitchPower(documentSnapshot, index);
        },
      ),
    );
  }

  Widget buildSwitchPower(DocumentSnapshot documentSnapshot, int index) {
    _consumptions = FirebaseFirestore.instance
        .collection('switches')
        .doc(documentSnapshot.id)
        .collection('consumption');
    return Column(
      children: [
        SizedBox(height: 20),
        InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => SwitchOverviewPage(
                        snapshot: documentSnapshot, index: index)));
          },
          child: Container(
            height: 150,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              color: Colors.grey[900],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text(
                      'Enérgie ${documentSnapshot['name']} #${index + 1}',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          fontWeight: FontWeight.bold)),
                ),
                SizedBox(height: 25),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Courant :',
                          style: TextStyle(
                              color: Colors.white54,
                              fontSize: 22,
                              fontWeight: FontWeight.bold)),
                      StreamBuilder(
                        stream: _consumptions
                            .where('privateKey',
                                isEqualTo: documentSnapshot['privateKey'])
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                                '${snapshot.data!.docs.last['current']} A',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold));
                          } else {
                            return Text('error A',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold));
                          }
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Voltage :',
                          style: TextStyle(
                              color: Colors.white54,
                              fontSize: 22,
                              fontWeight: FontWeight.bold)),
                      StreamBuilder(
                        stream: _consumptions
                            .where('privateKey',
                                isEqualTo: documentSnapshot['privateKey'])
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.hasData) {
                            return Text(
                                '${snapshot.data!.docs.last['voltage']} V',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold));
                          } else {
                            return Text('error V',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold));
                          }
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget buildSwitchHistory() {
    return Container(
      child: SfCartesianChart(
        title: ChartTitle(
            text: 'Consomation',
            textStyle: TextStyle(
                color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
            alignment: ChartAlignment.center),
        legend: Legend(
            isVisible: false,
            textStyle: TextStyle(color: Colors.white)),
        series: <ChartSeries>[
          StackedAreaSeries<ExpenseData, int>(
            color: Colors.green.withOpacity(0.65),
            name: 'intensité',
            dataSource: getChartData(),
            xValueMapper: (ExpenseData exp, _) => exp.time,
            yValueMapper: (ExpenseData exp, _) => exp.value,
          ),
          StackedAreaSeries<ExpenseData, int>(
            color: Colors.blue.withOpacity(0.65),
            name: 'tension',
            dataSource: getChartData(),
            xValueMapper: (ExpenseData exp, _) => exp.time,
            yValueMapper: (ExpenseData exp, _) => exp.value,
          ),
          StackedAreaSeries<ExpenseData, int>(
            color: Colors.purple.withOpacity(0.65),
            name: 'puissance',
            dataSource: getChartData(),
            xValueMapper: (ExpenseData exp, _) => exp.time,
            yValueMapper: (ExpenseData exp, _) => exp.value,
          )
        ],
        primaryXAxis: CategoryAxis(title: AxisTitle(text: 'Temps (heures)', textStyle: TextStyle(color: Colors.white))),
      ),
    );
  }

  List<ExpenseData> getChartData() {
    final List<ExpenseData> chartData = [
      ExpenseData(0, 35),
      ExpenseData(1, 28),
      ExpenseData(2, 34),
      ExpenseData(3, 32),
      ExpenseData(4, 40),
      ExpenseData(5, 40),
      ExpenseData(6, 50),
      ExpenseData(7, 50),
      ExpenseData(8, 50),
      ExpenseData(9, 50),
      ExpenseData(10, 50),
      ExpenseData(11, 50),
      ExpenseData(12, 50),
      ExpenseData(13, 50),
      ExpenseData(14, 50),
      ExpenseData(15, 50),
      ExpenseData(16, 50),
      ExpenseData(17, 50),
      ExpenseData(18, 150),
      ExpenseData(19, 50),
      ExpenseData(20, 50),
      ExpenseData(21, 50),
      ExpenseData(22, 50),
      ExpenseData(23, 50),
      ExpenseData(24, 200),
    ];
    return chartData;
  }

}

class ExpenseData {
  final int time;
  final num value;

  ExpenseData(this.time, this.value);
}
