import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smartswitch/controllers/switch_manager.dart';
import 'package:smartswitch/views/layouts/login_layout.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom]);
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          buildTitle(),
          SizedBox(height: 20),
          buildUnderdev(),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(top: 20, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  buildPhoneSettings(),
                  SizedBox(height: 40),
                  buildLogoutButton(),
                ],
              ),
            ),
          ),
        ],
      ),
    ));
  }

  Widget buildUnderdev() {
    return Center(
      child: Container(
        child: Column(
          children: [
            SizedBox(height: 20),
            Text(
              "Etat: en développement..",
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            Text(
              "Version: 1.0.0",
              style: TextStyle(
                color: Colors.white,
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildPhoneSettings() {
    return Center(
      child: Container(
        padding: EdgeInsets.only(top: 20, bottom: 20),
        height: 300,
        width: 300,
        decoration: BoxDecoration(
          color: Colors.grey[900],
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          children: [
            Text(
              "Informations du téléphone :",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            SizedBox(height: 15),
            Text(
              "ID : ${_deviceData['id']}",
              style: TextStyle(color: Colors.green, fontSize: 15),
            ),
            SizedBox(height: 15),
            Text(
              "Release : ${_deviceData['version.release']}",
              style: TextStyle(color: Colors.green, fontSize: 15),
            ),
            SizedBox(height: 15),
            Text(
              "Sdk : ${_deviceData['version.sdkInt']}",
              style: TextStyle(color: Colors.green, fontSize: 15),
            ),
            SizedBox(height: 15),
            Text(
              "SecurityPatch : ${_deviceData['version.securityPatch']}",
              style: TextStyle(color: Colors.green, fontSize: 15),
            ),
            SizedBox(height: 15),
            Text(
              "previewSdkInt : ${_deviceData['version.previewSdkInt']}",
              style: TextStyle(color: Colors.green, fontSize: 15),
            ),
            SizedBox(height: 15),
            Text(
              "Model : ${_deviceData['model']}",
              style: TextStyle(color: Colors.green, fontSize: 15),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildTitle() {
    return Center(
      child: Container(
        child: Text(
          "Paramètres",
          style: TextStyle(
            color: Colors.white,
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget buildLogoutButton() {
    return Center(
      child: InkWell(
        onTap: () => _logout(),
        child: Container(
          alignment: Alignment.center,
          height: 40,
          width: 200,
          decoration: BoxDecoration(
            color: Colors.grey.shade900,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Text(
            "Déconnexion",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }

  void _logout() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginPage()));
    SwitchManager().setBarHidden(true);
  }

  Future<void> initPlatformState() async {
    var deviceData = <String, dynamic>{};

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    setState(() {
      _deviceData = deviceData;
    });
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'systemFeatures': build.systemFeatures,
      'displaySizeInches':
          ((build.displayMetrics.sizeInches * 10).roundToDouble() / 10),
      'displayWidthPixels': build.displayMetrics.widthPx,
      'displayWidthInches': build.displayMetrics.widthInches,
      'displayHeightPixels': build.displayMetrics.heightPx,
      'displayHeightInches': build.displayMetrics.heightInches,
      'displayXDpi': build.displayMetrics.xDpi,
      'displayYDpi': build.displayMetrics.yDpi,
    };
  }
}
