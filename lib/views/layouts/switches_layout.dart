import 'dart:convert';

import 'package:azstore/azstore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:smartswitch/controllers/switch_manager.dart';
import 'package:smartswitch/views/layouts/addswitch_layout.dart';
import 'package:smartswitch/views/layouts/switch_overview_layout.dart';

class SwitchesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SwitchesPageState();
}

class _SwitchesPageState extends State<SwitchesPage> {
  final CollectionReference _switches =
      FirebaseFirestore.instance.collection('switches');

  @override
  void initState() {
    SwitchManager().setBarHidden(false);
    super.initState();

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: [SystemUiOverlay.bottom]);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.black,
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: buildFloatingAddButton(),
        body: Container(
          padding: EdgeInsets.only(left: 30, right: 30),
          child: Column(
            children: [
              buildPageTitle(),
              Expanded(
                child: StreamBuilder(
                    stream: _switches
                        .where('userId',
                            isEqualTo: FirebaseAuth.instance.currentUser!.uid)
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.docs.length,
                          itemBuilder: (context, index) {
                            final DocumentSnapshot documentSnapshot =
                                snapshot.data!.docs[index];
                            return buildSwitchCard(documentSnapshot, index);
                          },
                        );
                      } else {
                        return Center(
                          child: new CircularProgressIndicator(),
                        );
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildPageTitle() {
    return Container(
      padding: EdgeInsets.fromLTRB(50, 0, 0, 0),
      child: Center(
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8),
              child: Text(
                "●",
                style: TextStyle(fontSize: 40, color: Colors.lightGreenAccent),
              ),
            ),
            SizedBox(width: 10),
            Text("Prises en ligne",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 28,
                    fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }

  Widget buildSwitchCard(DocumentSnapshot snapshot, int index) {
    return Dismissible(
      key: Key(index.toString()),
      onDismissed: (direction) {
        delete(snapshot.id);
      },
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      SwitchOverviewPage(snapshot: snapshot, index: index)));
        },
        child: Padding(
          padding: EdgeInsets.only(top: index == 0 ? 0 : 25),
          child: Container(
            height: 50,
            width: 140,
            padding: EdgeInsets.only(left: 30, right: 30),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              color: Colors.white,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Center(
                    child: Text(
                  snapshot['name'],
                  style: TextStyle(fontSize: 25),
                )),
                Center(
                    child: Text(
                  '#${index + 1}',
                  style: TextStyle(fontSize: 25),
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildFloatingAddButton() {
    return FloatingActionButton.small(
        backgroundColor: Colors.white24,
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddSwitchPage()),
          );
        });
  }

  Future<void> delete(String id) async {
    await _switches.doc(id).delete();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(
      'Vous venez de supprimer la prise',
      textAlign: TextAlign.center,
    )));
  }
}
