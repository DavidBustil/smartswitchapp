import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smartswitch/app_theme.dart';
import 'package:smartswitch/controllers/switch_manager.dart';
import 'package:smartswitch/models/switch_item.dart';
import 'package:table_calendar/table_calendar.dart';

class AddSwitchPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AddSwitchPageState();
}

class _AddSwitchPageState extends State<AddSwitchPage> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _privateKeyController = TextEditingController();

  SwitchManager switchManager = new SwitchManager();

  DateTime today = DateTime.now();

  TimeOfDay? beginTime = const TimeOfDay(hour: 12, minute: 12);
  TimeOfDay? endTime = const TimeOfDay(hour: 12, minute: 12);

  late bool isActivated;

  void _onDaySelected(DateTime day, DateTime focusedDay) {
    setState(() {
      today = day;
    });
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [
      SystemUiOverlay.bottom
    ]);

    isActivated = false;
  }

  final CollectionReference _switches = FirebaseFirestore.instance.collection('switches');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.black54,
      body: StreamBuilder<Object>(
        stream: _switches.snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildPageTitle(),
                Container(
                  padding: EdgeInsets.only(left: 30, right: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      buildTextField("Nom", _nameController, double.infinity),
                      SizedBox(height: 5),

                      // Aligner sur la même ligne
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          buildTextField(
                              "Clé privé", _privateKeyController, 160),
                          SizedBox(width: 90),
                          buildSwitch(),
                        ],
                      ),
                      SizedBox(height: 10),
                      buildCalendar(),
                      SizedBox(height: 3),
                      buildProgTitle(),
                      SizedBox(height: 4),
                      buildTimeButton(),
                      SizedBox(height: 8),
                      buildValidateButton(),
                    ],
                  ),
                )
              ],
            );
          } else {
            return CircularProgressIndicator();
          }
        }
      ),
    );
  }

  Widget buildSwitch() {
    return Column(
      children: [
        Text(
          "Etat",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        SizedBox(height: 15),
        Transform.scale(
          scaleY: 1.6,
          scaleX: 1.6,
          child: CupertinoSwitch(
            activeColor: Colors.green,
            thumbColor: Colors.grey,
            trackColor: Colors.redAccent,
            value: isActivated,
            onChanged: (value) => setState(() => isActivated = value),
          ),
        ),
      ],
    );
  }

  Widget buildCalendar() {
    return Container(
      child: TableCalendar(
        selectedDayPredicate: (day) => isSameDay(day, today),
        focusedDay: today,
        firstDay: DateTime.now(),
        lastDay: DateTime.utc(2030),
        onDaySelected: _onDaySelected,
      ),
      decoration: BoxDecoration(
        color: AppTheme.grey,
        borderRadius: BorderRadius.all(Radius.circular(25)),
      ),
    );
  }

  Widget buildPageTitle() {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
      child: Center(
        child: Text("Configuration",
            style: TextStyle(
                color: Colors.white,
                fontSize: 28,
                fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget buildProgTitle() {
    return Container(
      child: Center(
        child: Text("Programmation",
            style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget buildNameField() {
    return TextFormField(
      controller: _nameController,
      decoration: InputDecoration(
          labelText: 'Nom',
          labelStyle: TextStyle(
              fontFamily: 'Montserrat',
              fontWeight: FontWeight.bold,
              color: Colors.white),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
          )),
    );
  }

  Widget buildTimeButton() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          children: [
            Text(
              "heure de début",
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            SizedBox(height: 2),
            Container(
              height: 40,
              width: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white,
              ),
              child: Center(
                child: InkWell(
                  onTap: () async {
                    TimeOfDay? newTime = await showTimePicker(
                        context: context, initialTime: beginTime!);
                    if (newTime != null) {
                      setState(() {
                        beginTime = newTime;
                      });
                    }
                  },
                  child: Text(
                    '${beginTime!.hour.toString()}:${beginTime!.minute.toString()}',
                    style: TextStyle(fontSize: 18, color: Colors.black),
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(width: 20),
        Column(
          children: [
            Text(
              "heure de fin",
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
            SizedBox(height: 2),
            Container(
              height: 40,
              width: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white,
              ),
              child: Center(
                child: InkWell(
                  onTap: () async {
                    TimeOfDay? newTime = await showTimePicker(
                        context: context, initialTime: endTime!);
                    if (newTime != null) {
                      setState(() {
                        endTime = newTime;
                      });
                    }
                  },
                  child: Text(
                    '${endTime!.hour.toString()}:${endTime!.minute.toString()}',
                    style: TextStyle(fontSize: 18, color: Colors.black),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildValidateButton() {
    return Center(
      child: InkWell(
        onTap: () {
          _addSwitchToDatabase();
          _scheduleSwitch();
          Navigator.of(context).pop();
        },
        child: Container(
          height: 40,
          width: 200,
          decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.all(Radius.circular(20)),
          ),
          child: Center(
              child: Text(
            "Valider",
            style: TextStyle(color: Colors.white, fontSize: 20),
          )),
        ),
      ),
    );
  }

  //Ajouté après
  Future<void> _scheduleSwitch() async {
    var data = {
      'name': 'scheduleTime,${today.day},${beginTime!.hour},${beginTime!.minute},${today.day},${endTime!.hour},${endTime!.minute}',
    };
    apiRequest('https://smartswitchmanager.azurewebsites.net/api/VOTRECLE', data);
  }

  Future<String> apiRequest(String url, Map jsonMap) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }

  Future<void> _addSwitchToDatabase() async {
    SwitchItem item = SwitchItem(
      name: _nameController.text,
      privateKey: _privateKeyController.text,
      status: isActivated,
      begin: beginTime!,
      end: endTime!,
      date: today,
      userId: FirebaseAuth.instance.currentUser!.uid,
    );

    await switchManager.addSwitchData(item.toJSON());
  }

  Widget buildTextField(String title, TextEditingController controller, double width) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          SizedBox(height: 10),
          Container(
            width: width,
            child: TextFormField(
              controller: controller,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'champ requis';
                }
                return null;
              },
              autofocus: false,
              decoration: InputDecoration(
                hintStyle: const TextStyle(
                    fontFamily: 'Open Sans',
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.normal,
                    color: Colors.grey),
                filled: true,
                fillColor: Colors.white,
                contentPadding: const EdgeInsets.all(13.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(12),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: const BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
