import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:azstore/azstore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:smartswitch/models/consumption_item.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class SwitchOverviewPage extends StatefulWidget {
  late DocumentSnapshot snapshot;
  late int index;

  SwitchOverviewPage({required DocumentSnapshot<Object?> snapshot, required int index}) {
    this.snapshot = snapshot;
    this.index = index;
  }

  @override
  State<StatefulWidget> createState() =>
      _SwitchOverviewPageState(snapshot: snapshot, index: index);
}

class _SwitchOverviewPageState extends State<SwitchOverviewPage> {

  Timer? timer;

  DocumentSnapshot snapshot;
  int index;

  late CollectionReference _consumptions;

  late TooltipBehavior _tooltipBehavior;

  late double _totalConsumption = 0;

  late List<ConsumptionItem> _consumptionItems = [];

  List<ExpenseData> _chartCurrentData = [];
  List<ExpenseData> _chartVoltageData = [];
  List<ExpenseData> _chartPowerData = [];

  @override
  void initState() {
    super.initState();

    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [
      SystemUiOverlay.bottom
    ]);

    _tooltipBehavior = TooltipBehavior(enable: true);

    _consumptions = FirebaseFirestore.instance
        .collection('switches')
        .doc(snapshot.id)
        .collection('consumption');

    refreshData();
    timer = Timer.periodic(Duration(seconds: 15), (Timer t) => refreshData());
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  _SwitchOverviewPageState({required this.snapshot, required this.index});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Container(
          padding: EdgeInsets.only(left: 30, right: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // SizedBox(height: 30),
              buildSwitchName(),

              Expanded(
                child: StreamBuilder(
                    stream: _consumptions
                        .where('privateKey', isEqualTo: snapshot['privateKey'])
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: snapshot.data!.docs.length,
                          itemBuilder: (context, index) {
                            final DocumentSnapshot documentSnapshot =
                                snapshot.data!.docs[index];
                            return buildInterface(documentSnapshot, index);
                          },
                        );
                      } else {
                        return Center(
                          child: new CircularProgressIndicator(),
                        );
                      }
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildInterface(DocumentSnapshot documentSnapshot, int index) {
    return Column(
      children: [
        SizedBox(height: 10),
        buildSwitchStatus(documentSnapshot),
        SizedBox(height: 20),
        buildSwitchPower(documentSnapshot),
        SizedBox(height: 10),
        buildSwitchHistory(documentSnapshot),
      ],
    );
  }

  Widget buildSwitchName() {
    return Container(
      child: Center(
        child: Text('${snapshot['name']} #${index + 1}',
            style: TextStyle(
                color: Colors.white,
                fontSize: 28,
                fontWeight: FontWeight.bold)),
      ),
    );
  }

  Widget buildSwitchStatus(DocumentSnapshot documentSnapshot) {
    String voltage = documentSnapshot['voltage'].toString();
    String current = documentSnapshot['current'].toString();

    double voltageDouble = double.parse(voltage);
    double currentDouble = double.parse(current);

    double power = voltageDouble * currentDouble;

    return Container(
      height: 140,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        color: Colors.grey[900],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Total consomée',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24,
                        fontWeight: FontWeight.bold)),
                Text(
                  "Etat",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('${(_totalConsumption/1000).toStringAsFixed(2)} kWh',
                    style: TextStyle(
                        color: Colors.green,
                        fontSize: 40,
                        fontWeight: FontWeight.bold)),
                buildSwitch(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSwitch() {
    return Container(
      child: Transform.scale(
        scaleY: 1.6,
        scaleX: 1.6,
        child: CupertinoSwitch(
          activeColor: Colors.green,
          thumbColor: Colors.grey,
          trackColor: Colors.redAccent,
          value: status,
          onChanged: (value) => setState(() { status = value;
            toggleSwitch();
          }),
        ),
      ),
    );
  }

  Widget buildSwitchPower(DocumentSnapshot documentSnapshot) {
    String voltage = documentSnapshot['voltage'].toString();
    String current = documentSnapshot['current'].toString();

    return Container(
      height: 200,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        color: Colors.grey[900],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Text('Consomation temps réel',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 25),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Courant :',
                        style: TextStyle(
                            color: Colors.white54,
                            fontSize: 22,
                            fontWeight: FontWeight.bold)),
                    Text('${_consumptionItems.isNotEmpty ? _consumptionItems.last.current : 0} A',
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 22,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Tension :',
                        style: TextStyle(
                            color: Colors.white54,
                            fontSize: 22,
                            fontWeight: FontWeight.bold)),
                    Text('${_consumptionItems.isNotEmpty ? _consumptionItems.last.voltage : 0} V',
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 22,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
                SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Puissance :',
                        style: TextStyle(
                            color: Colors.white54,
                            fontSize: 22,
                            fontWeight: FontWeight.bold)),
                    Text('${_consumptionItems.isNotEmpty ? _consumptionItems.last.power : 0} Watts',
                        style: TextStyle(
                            color: Colors.green,
                            fontSize: 22,
                            fontWeight: FontWeight.bold)),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildSwitchHistory(DocumentSnapshot documentSnapshot) {

    _chartCurrentData.add(new ExpenseData(index, double.parse(documentSnapshot['current'].toString())));

    return Container(
      child: SfCartesianChart(
        tooltipBehavior: _tooltipBehavior,
        title: ChartTitle(
            text: 'Consomation',
            textStyle: TextStyle(
                color: Colors.white, fontSize: 15, fontWeight: FontWeight.bold),
            alignment: ChartAlignment.center),
        legend: Legend(
            isVisible: false,
            textStyle: TextStyle(color: Colors.white)),
        series: <ChartSeries>[
          StackedAreaSeries<ExpenseData, num>(
            color: Colors.green.withOpacity(0.65),
            name: 'intensité',
            dataSource: _chartCurrentData,
            xValueMapper: (ExpenseData exp, _) => exp.time,
            yValueMapper: (ExpenseData exp, _) => exp.value,
          ),
          StackedAreaSeries<ExpenseData, int>(
            color: Colors.blue.withOpacity(0.65),
            name: 'tension',
            dataSource: _chartVoltageData,
            xValueMapper: (ExpenseData exp, _) => exp.time,
            yValueMapper: (ExpenseData exp, _) => exp.value,
          ),
          StackedAreaSeries<ExpenseData, int>(
            color: Colors.purple.withOpacity(0.65),
            name: 'puissance',
            dataSource: _chartPowerData,
            xValueMapper: (ExpenseData exp, _) => exp.time,
            yValueMapper: (ExpenseData exp, _) => exp.value,
          )
        ],
        primaryXAxis: CategoryAxis(title: AxisTitle(text: 'Temps (heures)', textStyle: TextStyle(color: Colors.white))),
      ),
    );
  }

  Future<List<ConsumptionItem>> showBlobStorage() async {
    final _connectionString = "DefaultEndpointsProtocol=https;AccountName=smartswitchstorage;VOTRECLEDECONNEXION";
    var storage = AzureStorage.parse(_connectionString);

    var result = await storage.getBlob('/smartswitch/smartswitch/0_af2191ef7ae041d5ba520f9b68f15c00_1.json').asStream().last;
    final response = await result.stream.bytesToString();

    final firsChar = '[';
    final lastChar = '{}]';

    final body = firsChar + response.toString().replaceAll("}}", "}},") + lastChar;
    final json = jsonDecode(body);

    List<ConsumptionItem> items = [];

    for (var item in json) {
      if (item['voltage'] == null) continue;

      bool state = item['state'].toString() == "true" ? true : false;

      String voltageStr = item['voltage'].toString();
      String currentStr = item['current'].toString();
      String powerStr = item['power'].toString();

      double voltage = double.parse(voltageStr);
      double current = double.parse(currentStr);
      double power = double.parse(powerStr);

      items.add(ConsumptionItem(state, voltage, current, power));
    }

    status = items[items.length - 1].state;

    return items;
  }

  bool status = false;

  Future<void> toggleSwitch() async {
    var data = {
      'name': 'toggleSwitch,${status}'
    };
    apiRequest('https://smartswitchmanager.azurewebsites.net/api/VOTRECLEDAPI', data);
  }

  Future<String> apiRequest(String url, Map jsonMap) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    // todo - check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }

  refreshData() async {
    _consumptionItems.clear();

    _consumptionItems = await showBlobStorage();

    _chartCurrentData.clear();
    _chartVoltageData.clear();
    _chartPowerData.clear();

    int hour = 1;

    for (var item in _consumptionItems) {
      _chartCurrentData.add(new ExpenseData(hour, double.parse(item.current.toString())));
      _chartVoltageData.add(new ExpenseData(hour, double.parse(item.voltage.toString())));
      _chartPowerData.add(new ExpenseData(hour, double.parse(item.power.toString())));
      _totalConsumption += item.power;
      hour++;
    }

    setState(() {
    });
  }
}

class ExpenseData {
  final int time;
  final num value;

  ExpenseData(this.time, this.value);
}
