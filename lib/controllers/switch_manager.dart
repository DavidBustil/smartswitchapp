import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SwitchManager {
  static final SwitchManager _instance = SwitchManager._internal();
  factory SwitchManager() => _instance;
  SwitchManager._internal();

  final CollectionReference _switches = FirebaseFirestore.instance.collection('switches');

  bool isBarHidden = false;

  bool getBarHidden() {
    return isLoggedIn() ? false : isBarHidden ? false : true;
  }

  setBarHidden(bool value) {
      isBarHidden = value;
  }

  bool isLoggedIn() {
    if (FirebaseAuth.instance.currentUser != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> addSwitchData(switchData) async {
    if (isLoggedIn()) {
      Map<String, dynamic> switchConsumption = {
        'privateKey': switchData['privateKey'],
        'voltage': 0,
        'current': 0,
        'power': 0,
      };
      DocumentReference<Object?> reference =
          await _switches.add(switchData).catchError((e) => print(e));
      await _switches
          .doc(reference.id)
          .collection('consumption')
          .add(switchConsumption)
          .catchError((e) => print(e));
      await _switches
          .doc(reference.id)
          .collection('historic')
          .add(switchConsumption)
          .catchError((e) => print(e));
    } else {
      print("You need to be logged in");
    }
  }

  Future<void> updateSwitchData(selectedDoc, newValues) async {
    _switches.doc(selectedDoc).update(newValues).catchError((e) => print(e));
    print('updated');
  }

  Future<void> deleteSwitchData(docId) async {
    _switches.doc(docId).delete().catchError((e) => print(e));
    print('deleted');
  }
}
