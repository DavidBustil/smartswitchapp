import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SwitchItem {
  String name;
  String privateKey;
  String userId;

  bool status;

  DateTime date;

  TimeOfDay begin;
  TimeOfDay end;

  SwitchItem({required this.name, required this.privateKey,required this.status, required this.date, required this.begin, required this.end, required this.userId});

  Map<String, dynamic> toJSON() {
    return {
      'name': name,
      'privateKey': privateKey,
      'status': status,
      'date': date.toString(),
      'begin': begin.toString(),
      'end': end.toString(),
      'userId': userId,
    };
  }

  fromJSON(DocumentSnapshot doc) {
    name = doc['name'];
    privateKey = doc['privateKey'];
    status = doc['status'];
    date = DateTime(doc['date']);
    begin = doc['begin'];
    end = doc['end'];
    userId = doc['userId'];
  }
}
