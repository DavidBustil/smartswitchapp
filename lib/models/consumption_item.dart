class ConsumptionItem {
  bool state;
  double voltage;
  double current;
  double power;

  ConsumptionItem(this.state, this.voltage, this.current, this.power);
}